# Generated by Django 2.0.6 on 2018-08-31 21:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0008_auto_20180901_0318'),
    ]

    operations = [
        migrations.AlterField(
            model_name='participation',
            name='quiz_allot',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='quiz.Quiz'),
        ),
    ]
