from django.conf.urls import url
from . import views

urlpatterns = [
    url('home/$',views.home,name='home'),
    url('contest/quiz/$', views.contest_page, name='question'),
    url('contest/quiz1/(?P<quesno>\w+)/$', views.ques_page, name='questionpage'),
    url('login_user/$',views.login_user,name='login-user'),
    url('questions/$', views.question, name='question'),
    url('answer/submit/$', views.submitans, name='submit-answer'),
    url('logout/$', views.logoutuser, name='logout'),
    url('register/$', views.register, name='register'),
    url('scorecard/$', views.scorecard, name='logout'),
    url('rules/$', views.rules, name='rules'),
    url(r"^activate/b'(?P<uidb64>[0-9A-Za-z_\-]+)'/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$", views.activate,name='activate'),
    url(r'^$', views.sendhome,name='redirect'),
    url(r'^endtest/$', views.endtest, name='end-test'),

]
