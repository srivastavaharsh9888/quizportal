from django.contrib import admin

from quiz.models import Types,Ques_ans,QuizScore,Quiz,Contest,OnGoingcontest,Participation

admin.site.register(Types)
admin.site.register(Ques_ans)
admin.site.register(QuizScore)
admin.site.register(Quiz)
admin.site.register(Contest)
admin.site.register(OnGoingcontest)
admin.site.register(Participation)
