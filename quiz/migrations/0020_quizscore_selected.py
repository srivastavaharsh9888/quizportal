# Generated by Django 2.0.6 on 2018-09-14 11:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0019_auto_20180908_2143'),
    ]

    operations = [
        migrations.AddField(
            model_name='quizscore',
            name='selected',
            field=models.CharField(default='', max_length=1),
        ),
    ]
